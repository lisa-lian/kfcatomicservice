
代码仓地址：https://gitee.com/lisa-lian/kfcatomicservice

1，配置签名。修改代码仓内的clientID和包名。
https://developer.huawei.com/consumer/cn/doc/harmonyos-guides/account-certificate-fingerprint-0000001607795142#section16919125173215
https://developer.huawei.com/consumer/cn/doc/harmonyos-guides/account-client-id-0000001658658849#section41131234501
2，配置隐私授权弹框。
https://developer.huawei.com/consumer/cn/doc/app/agc-help-harmonyos-privacystatement-0000001645256448
3，配置华为账号登录权限。
https://developer.huawei.com/consumer/cn/doc/harmonyos-guides/account-get-phonenumber-0000001601012050