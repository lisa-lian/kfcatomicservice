import request from './request';
import { CouponItemInfo } from '../interface/CouponPageInterface';
import dayjs from 'dayjs';
import { http } from '@kit.NetworkKit';
import { LoginDataManager } from './LoginDataManager';
import { Log } from '../util/Log';

const URI = 'https://tappcoupon.kfc.com.cn/api/crm/queryAvailableCoupon';
// 渠道id
const channelId = 210176;
const TAG = 'couponPageModelClass';
const token = '1c810188178e3350_190104f6802_190104f6802_lrt1KofVRzqFZO4Hs38RKA'; // 调试token

class couponPageModelClass {
  public async requestPageList(): Promise<CouponItemInfo[]> {
    let loginDataManager = LoginDataManager.getInstance();
    // let token = loginDataManager.getTokenData();  // 目前账号无数据，暂时使用调试token
    if (!token) {
      Log.showError(TAG, 'requestPageList token is empty');
      return;
    }
    let res = await request(URI, '/crm/queryAvailableCoupon', http.RequestMethod.GET, {
      token,
    }) as Record<string, Record<string, string | object>>;
    if (!res) {
      return null;
    }
    let remote = res?.data?.remote as Record<string, object>;
    if (!remote || !remote?.availableCouponInfos) {
      return null;
    }
    return this.formatPageData(remote.availableCouponInfos as object[]);
  }

  private formatPageData(data: object[]): CouponItemInfo[] {
    let result: CouponItemInfo[] = [];
    if (data?.length === 0) {
      Log.showError(TAG, 'formatPageData remote.availableCouponInfos length is 0');
      return result;
    }
    // 一期最多展示最新三个优惠券
    let sortData = data.length > 3 ?
    data.sort((a: Record<string, number>, b: Record<string, number>) => b.validStartTime - a.validStartTime)
      .slice(0, 3) :
      data;
    sortData.forEach((item: Record<string, string>) => {
      let resItem: CouponItemInfo = {
        image: item.pictureUrl || '',
        title: item.activity || '',
        time: this.getTime(item.validStartTime, item.validEndTime) || '',
        count: this.getUseCount(item.reuseableCount, item.usedSeqId),
        bottomTipList: this.getCouponTags(item.couponTags)
      }
      result.push(resItem);
    })
    return result;
  }

  private getCouponTags(tagsStr: string): string[] {
    let tagLists: string[] = []
    if (tagsStr) {
      let tagsOjb = JSON.parse(tagsStr);
      let objValue = Object.values(tagsOjb) as string[];
      if (objValue && objValue.length > 0) {
        tagLists = [...objValue];
      }
    }
    return tagLists.slice(0,2);
  }

  private getUseCount(totalCount, usedCount) {
    let canUseCount = totalCount - usedCount;
    if (canUseCount < 0) {
      canUseCount = 0
    }
    return canUseCount;
  }

  private getTime(start: string, end: string): string {
    if (!start || !end) {
      return '';
    }
    let startDate = new Date(start);
    let endDate = new Date(end);
    let startTime = dayjs(startDate).format('YYYY.MM.DD');
    let endTime = dayjs(endDate).format('YYYY.MM.DD');
    return startTime + '-' + endTime
  }
}

let couponPageModel = new couponPageModelClass()

export default couponPageModel;