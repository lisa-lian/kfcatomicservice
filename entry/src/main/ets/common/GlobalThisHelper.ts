import type common from '@ohos.app.ability.common';
import context from '@kit.AbilityKit';

export const enum GlobalThisKey {
  // 住窗口对象
  MAIN_WINDOW = 'mainWindow',

  // 住窗口对象
  CONTEXT = 'context'
}

export class GlobalThisHelper {
  public static set<T>(key: GlobalThisKey, value: T): void {
    globalThis[key] = value;
  }

  public static get<T>(key: GlobalThisKey): T {
    return (globalThis[key] as T)
  }
}