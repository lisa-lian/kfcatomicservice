import { authentication } from '@kit.AccountKit';
import request from './request';
import { BusinessError } from '@kit.BasicServicesKit';
import { bundleManager, common } from '@kit.AbilityKit';
import { http } from '@kit.NetworkKit';
import { GlobalThisHelper, GlobalThisKey } from './GlobalThisHelper';
import { Log } from '../util/Log';

const TAG = 'LoginDataManager';

export enum LoginEvent {
  LOGIN_SUCCESS,
  LOGIN_LOADING
}

export class LoginDataManager {
  private static sInstance: LoginDataManager = null;
  private tokenData: string = '';
  private userNickname: string = '';
  private userPhoto: string = '';
  private loginEventMap: Map<LoginEvent, Function> = new Map();

  public static getInstance(): LoginDataManager {
    if (LoginDataManager.sInstance == null) {
      LoginDataManager.sInstance = new LoginDataManager();
    }
    return LoginDataManager.sInstance;
  }

  public getTokenData(): string {
    return this.tokenData;
  }

  public getUserNickname(): string {
    return this.userNickname;
  }

  public getUserPhoto(): string {
    return this.userPhoto;
  }

  public notifyLoginEvent(eventId: LoginEvent, result: boolean|string) {
    if (!this.loginEventMap.has(eventId)) {
      Log.showError(TAG, `no event ${eventId} has registered`);
      return;
    }
    this.loginEventMap.get(eventId)(result);
  }

  public registerLoginEvent(eventId:LoginEvent, callback:Function) {
    if (this.loginEventMap.has(eventId)) {
      Log.showError(TAG, `event ${eventId} has already registered`);
      return;
    }
    this.loginEventMap.set(eventId, callback);
  }

  public unRegisterLoginEvent(eventId:LoginEvent) {
    if (!this.loginEventMap.has(eventId)) {
      Log.showError(TAG, `no event ${eventId} has registered`);
      return;
    }
    this.loginEventMap.delete(eventId);
  }

  public getQuickLogin(context: common.UIAbilityContext) {
    // 创建授权请求，并设置参数
    let authRequest = new authentication.HuaweiIDProvider().createAuthorizationWithHuaweiIDRequest();
    // 获取手机号需要传如下scope，传参数之前需要先申请对应scope权限,才能返回对应数据
    authRequest.scopes = ['phone'];
    // 获取code需传如下permission
    authRequest.permissions = ['serviceauthcode'];
    // 用户是否需要登录授权，该值为true且用户未登录或未授权时，会拉起用户登录或授权页面
    authRequest.forceAuthorization = true;
    // 用于防跨站点请求伪造，非空字符串即可
    authRequest.state = '1111';
    try {
      let controller = new authentication.AuthenticationController(context);
      controller.executeRequest(authRequest, async (err, data) => {
        if (err) {
          Log.showError(TAG, `getQuickLogin auth fail,error: ${JSON.stringify(err)}`);
          context.terminateSelf();
          return;
        }
        let authorizationWithHuaweiIDResponse = data as authentication.AuthorizationWithHuaweiIDResponse;
        let state = authorizationWithHuaweiIDResponse.state;
        if (state != undefined && authRequest.state != state) {
          Log.showError(TAG,
            `getQuickLogin auth fail,The state is different: ${JSON.stringify(authorizationWithHuaweiIDResponse)}`);
          return;
        }
        Log.showInfo(TAG, `auth success: ${JSON.stringify(authorizationWithHuaweiIDResponse)}`);
        let authorizationWithHuaweiIDCredential = authorizationWithHuaweiIDResponse.data!;
        let code = authorizationWithHuaweiIDCredential.authorizationCode;
        let res = await request('https://tauthlogin.kfc.com.cn/api/user/login/phone', '/user/login/phone',
          http.RequestMethod.POST,
          {
            accessCode: code,
            hwPlatform: 'hmsMobile'
          }) as Record<string, Record<string, string | object>>;
        this.tokenData = res.data?.token as string;
        this.userPhoto = res.data?.photo as string;
        this.userNickname = res.data?.nickname as string;
        AppStorage.set('isShowOrder', true);
        // 开发者处理code
      });
    } catch (e) {
      let error = e as BusinessError;
      Log.showError(TAG, `getQuickLogin auth fail: code ${error?.code} message ${error?.message}`);
    }
  }

  // 错误处理
  dealAllError(error: BusinessError): void {
  Log.showError(TAG, `Failed, errorCode error: code ${error?.code} message: ${error?.message}`);
  }
}