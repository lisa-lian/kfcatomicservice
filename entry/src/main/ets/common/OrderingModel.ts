import request from './request';
import dayjs from 'dayjs';
import { orderItemInfo } from '../interface/OrderPageInterface';
import { http } from '@kit.NetworkKit';
import { LoginDataManager } from './LoginDataManager';
import { Log } from '../util/Log';

const URI = 'https://tapporder.kfc.com.cn/api/order/queryOrderList';
const brand = 'KFC_PRE';
const TAG = 'OrderingModelClass';
const token = '1c810188178e3350_190104f6802_190104f6802_lrt1KofVRzqFZO4Hs38RKA'; // 调试token

class OrderingModelClass {
  public async requestPageList(): Promise<orderItemInfo[]> {
    let loginDataManager = LoginDataManager.getInstance();
    // let token = loginDataManager.getTokenData();   // 目前账号无数据，暂时使用调试token
    if (!token) {
      Log.showError(TAG, 'requestPageList token is empty');
      return;
    }
    let res = await request(URI, '/order/queryOrderList', http.RequestMethod.POST, {
      token,
      brand
    }) as Record<string, Record<string, string | object>>;
    if (!res) {
      return null;
    }
    let resList = res?.data?.datas as object[];
    if (!resList || resList.length === 0) {
      return null;
    }
    return this.formatPageData(resList as object[]);
  }

  private formatPageData(data: object[]): orderItemInfo[] {
    let result: orderItemInfo[] = [];
    if (data?.length === 0) {
      return result;
    }
    // 一期最多展示3个订单且按下单时间倒序
    let sortData =
      data.sort((a: Record<string, number>, b: Record<string, number>) => b.orderDate - a.orderDate).slice(0, 3);
    sortData.forEach((order: Record<string, string | string[] | number | object>) => {
      let imageDomain = order?.imageDomain as string || '';
      let resItem: orderItemInfo = {
        status: order?.status as string || '',
        statusName: order?.statusName as string || '',
        statusShortName: order?.statusShortName as string || '',
        imageList: this.getImageList(imageDomain, order?.items as Record<string, string>[]),
        title: order?.storeName as string || '',
        time: this.getTime(order?.orderDate as number),
        posOrderNumber: order?.posOrderNumber as string || '',
        total: order?.itemCount as number || 0,
        price: `${order?.userTenderAmount || ''}`
      }
      result.push(resItem);
    })
    return result;
  }

  private getImageList(imageDomain: string, productList: Record<string, string>[]): string[] {
    let imageList: string[] = []
    if (!imageDomain) {
      console.error(TAG, 'getImageList order iamgeDomain is empty');
      return imageList;
    }
    if (!productList && productList.length === 0) {
      return imageList;
    }
    productList.forEach(item => {
      if (item.imageName) {
        imageList.push(imageDomain + item.imageName);
      }
    })
    return imageList;
  }

  private getTime(ts: number): string {
    if (!ts) {
      return '';
    }
    let timeDate = new Date(ts);
    let time = dayjs(timeDate).format('YYYY.MM.DD HH:mm:ss');
    return time
  }
}

let orderPageModel = new OrderingModelClass();

export enum orderTypeEnum {
  PREPARING = '212',
  PIKMEAL = '214',
  COMPLETE = '215',
  CANCEL = '503'
}

export default orderPageModel;