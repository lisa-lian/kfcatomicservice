import http from '@ohos.net.http';
import { BusinessError } from '@kit.BasicServicesKit';
import { cryptoFramework } from '@kit.CryptoArchitectureKit';
import { buffer } from '@kit.ArkTS';
import { Log } from '../util/Log';

const TAG = 'REQUEST';
const KBCK = 'ylcfwJG0QlGsqkeV';
const CLIENTSEC = 'kPxafyx8HWPPPdTO';

export default async function request(url: string, path: string, method: http.RequestMethod, opt?: object) {
  let ts = Date.now();
  let httpRequest = http.createHttp();
  let md5kbsv = await getMd5Kdsv(path, method, ts, opt);
  Log.showInfo(TAG, `md5kbsv: ${md5kbsv}`);
  let options: http.HttpRequestOptions = {
    method: method,
    header: {
      kbck: KBCK,
      kbcts: ts.toString(),
      kbsv: md5kbsv
    },
    extraData: opt ? {
      ...opt
    } : {}
  };
  Log.showInfo(TAG, url, JSON.stringify(options));
  return httpRequest.request(url, options).then((res: http.HttpResponse) => {
    Log.showInfo(TAG, `responseCode: ${res.responseCode}`);
    if (res.responseCode === http.ResponseCode.OK) {
      let result = JSON.parse(res.result as string);
      Log.showInfo(TAG, res.result as string);
      return result;
    }
    return null;
  }).catch((error: BusinessError) => {
    Log.showError(TAG, `error code:${error.code} message: ${error.message}`);
    return null;
  })
}

// MD5加密
async function getMd5Kdsv(path: string, method:string, ts: number, opt?: object) {
  let message = '';
  if (opt) {
    let getParams = ''
    if (method === http.RequestMethod.GET) {
      // 属性升序
      let sortedKey = Object.keys(opt).sort();
      getParams = opt ? sortedKey.map(key => `${key}=${opt[key]}`).join('&') : '';
      message = KBCK + '\t' + CLIENTSEC + '\t' + ts + '\t' + path + '\t' + getParams;
    } else if (method === http.RequestMethod.POST) {
      let uriParams = ''
      getParams = JSON.stringify(opt);
      message = KBCK + '\t' + CLIENTSEC + '\t' + ts + '\t' + path + '\t' + uriParams + '\t' + getParams;
    }
  }
  Log.showInfo(TAG, `getMd5Kdsv message: ${message}`);
  try {
    let mdMsg = new Uint8Array(buffer.from(message, 'utf-8').buffer)
    let md = cryptoFramework.createMd('MD5');
    await md.update({ data: mdMsg });
    let mdDigest = await md.digest();
    // 转换32位签名
    if (mdDigest.data) {
      return uint8ArrayToHexStr(mdDigest.data);
    }
    return '';
  } catch (error) {
    let e: BusinessError = error as BusinessError;
    Log.showError(TAG, `getMd5Kdsv error code ${e?.code}, message ${e?.message}`);
  }
}

function uint8ArrayToHexStr(data: Uint8Array): string {
  let hexString = '';
  for (let i = 0; i < data.length; i++) {
    let char = ('00' + data[i].toString(16)).slice(-2);
    hexString += char
  }
  return hexString
}