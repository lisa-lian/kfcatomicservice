export interface CouponItemInfo {
  image: string;
  title: string;
  count: number | string;
  bottomTipList?: string[];
  time: string;
}