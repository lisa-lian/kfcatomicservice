export interface orderItemInfo {
  status: string;
  statusName?: string;
  statusShortName?: string;
  title: string;
  time: string;
  posOrderNumber: string;
  imageList: string[];
  price: string;
  total: number;
}