import { hilog } from '@kit.PerformanceAnalysisKit';

const SYMBOL = '-->';
const DOMAIN: number = 0X001b;


export class Log {
  private static TAG: string = 'KFCAtomicService';

  static setTag(tag: string) {
    Log.TAG = Log.TAG + '_' + tag;
  }

  private static isLoggable(tag: string, level: hilog.LogLevel): boolean {
    return hilog.isLoggable(DOMAIN, tag, level);
  }

  static showInfo(tag: string, format: string, ...arg: string[]): void {
    if (Log.isLoggable(tag, hilog.LogLevel.INFO)) {
      hilog.info(DOMAIN, Log.TAG, tag + SYMBOL + format, arg);
    }
  }

  static showWarn(tag: string, format: string, ...arg: string[]): void {
    if (Log.isLoggable(tag, hilog.LogLevel.WARN)) {
      hilog.warn(DOMAIN, Log.TAG, tag + SYMBOL + format, arg);
    }
  }

  static showError(tag: string, format: string, ...arg: string[]): void {
    if (Log.isLoggable(tag, hilog.LogLevel.ERROR)) {
      hilog.error(DOMAIN, Log.TAG, tag + SYMBOL + format, arg);
    }
  }
}